/*
SQLyog Community v12.02 (32 bit)
MySQL - 5.5.29 : Database - prioritize
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`prioritize` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `prioritize`;

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `fname` varchar(200) DEFAULT NULL,
  `fkey` varchar(200) DEFAULT NULL,
  `content` text,
  `sto` text,
  `ste` text,
  `fre` text,
  `cate` text,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `file` */

insert  into `file`(`id`,`fname`,`fkey`,`content`,`sto`,`ste`,`fre`,`cate`) values (1,'Os.txt','30285261','An operating system (OS) is system software that manages computer hardware and software resources and provides common services for computer programs. Time-sharing operating systems schedule tasks for efficient use of the system and may also include accounting software for cost allocation of processor time, mass storage, printing, and other resources.','operating system os system software manages computer hardware software resources provides common services computer programs time-sharing operating systems schedule tasks efficient use system include accounting software cost allocation processor time mass storage printing resources','[operat system os system software manages computer hardware software resources provides common services computer programs time-shar operat systems schedule tasks efficient use system include account software cost allocate processor time mass storage print resources]','{account=1, allocate=1, common=1, computer=2, cost=1, efficient=1, hardware=1, include=1, manages=1, mass=1, operat=2, os=1, print=1, processor=1, programs=1, provides=1, resources=2, schedule=1, services=1, software=3, storage=1, system=3, systems=1, tasks=1, time=1, time-shar=1, use=1}','Os'),(2,'single and multiuser.txt','78800584','Single-user operating systems have no facilities to distinguish users, but may allow multiple programs to run in tandem.[6] A multi-user operating system extends the basic concept of multi-tasking with facilities that identify processes and resources, such as disk space, belonging to multiple users, and the system permits multiple users to interact with the system at the same time. Time-sharing operating systems schedule tasks for efficient use of the system and may also include accounting software for cost allocation of processor time, mass storage, printing, and other resources to multiple users.','single-user operating systems facilities distinguish users allow multiple programs run tandem6 multi-user operating system extends basic concept multi-tasking facilities identify processes resources disk space belonging multiple users system permits multiple users interact system time time-sharing operating systems schedule tasks efficient use system include accounting software cost allocation processor time mass storage printing resources multiple users','[single-user operat systems facilities distinguish user allow multiple programs run tandem6 multi-user operat system extends basic concept multi-tasking facilities identify processes resources disk space belong multiple user system permits multiple user interact system time time-shar operat systems schedule tasks efficient use system include account software cost allocate processor time mass storage print resources multiple user]','{account=1, allocate=1, allow=1, basic=1, belong=1, concept=1, cost=1, disk=1, distinguish=1, efficient=1, extends=1, facilities=2, identify=1, include=1, interact=1, mass=1, multi-tasking=1, multi-user=1, multiple=4, operat=3, permits=1, print=1, processes=1, processor=1, programs=1, resources=2, run=1, schedule=1, single-user=1, software=1, space=1, storage=1, system=4, systems=2, tandem6=1, tasks=1, time=2, time-shar=1, use=1, user=4}','Operating System'),(3,'single tasking.txt','754454','A single-tasking system can only run one program at a time, while a multi-tasking operating system allows more than one program to be running in concurrency. This is achieved by time-sharing, dividing the available processor time between multiple processes that are each interrupted repeatedly in time slices by a task-scheduling subsystem of the operating system. Multi-tasking may be characterized in preemptive and co-operative types.','single-tasking system run one program time multi-tasking operating system allows one program running concurrency achieved time-sharing dividing available processor time multiple processes interrupted repeatedly time slices task-scheduling subsystem operating system multi-tasking characterized preemptive co-operative types','[single-tasking system run one program time multi-tasking operat system allows one program run concurrency achieved time-shar divid available processor time multiple processes interrupted repeatedly time slices task-schedul subsystem operat system multi-tasking characterized preemptive co-oper types]','{achieved=1, allows=1, available=1, characterized=1, co-oper=1, concurrency=1, divid=1, interrupted=1, multi-tasking=2, multiple=1, one=2, operat=2, preemptive=1, processes=1, processor=1, program=2, repeatedly=1, run=2, single-tasking=1, slices=1, subsystem=1, system=3, task-schedul=1, time=3, time-shar=1, types=1}','Operating System'),(4,'system software.txt','94303557','System software, or systems software, is computer software designed to provide a platform to other software.[1] Examples of system software include operating systems, computational science software, game engines, industrial automation, and software as a service applications. In contrast to system software, software that allows users to do things like create text documents, play games, listen to music, or web browsers to surf the web is called application software','system software systems software computer software designed provide platform software1 examples system software include operating systems computational science software game engines industrial automation software service applications contrast system software software allows users things create text documents play games listen music web browsers surf web called application software','[system software systems software computer software designed provide platform software1 examples system software include operat systems computation science software game engines industrial automate software service applications contrast system software software allows user th create text documents play games listen music web browser surf web called applicate software]','{allows=1, applicate=1, applications=1, automate=1, browser=1, called=1, computation=1, computer=1, contrast=1, create=1, designed=1, documents=1, engines=1, examples=1, game=1, games=1, include=1, industrial=1, listen=1, music=1, operat=1, platform=1, play=1, provide=1, science=1, service=1, software=9, software1=1, surf=1, system=3, systems=2, text=1, th=1, user=1, web=2}','Os'),(5,'crime.txt','23937091','Cyber crime, or computer oriented crime, is crime that involves a computer and a network.The computer may have been used in the commission of a crime, or it may be the target. Cyber crimes can be defined as: \"Offences that are committed against individuals or groups of individuals with a criminal motive to intentionally harm the reputation of the victim or cause physical or mental harm, or loss, to the victim directly or indirectly, using modern telecommunication networks such as Internet (networks including but not limited to Chat rooms, emails, notice boards and groups)','cyber crime computer oriented crime crime involves computer networkthe computer used commission crime target cyber crimes defined as offences committed individuals groups individuals criminal motive intentionally harm reputation victim cause physical mental harm loss victim directly indirectly using modern telecommunication networks internet networks including limited chat rooms emails notice boards groups','[cyber crime computer oriented crime crime involves computer networkthe computer used commission crime target cyber crimes defined as offences commit individuals groups individuals criminal motive intentionally harm reputate victim cause physic mental harm loss victim directly indirectly use modern telecommunicate networks internet networks includ limited chate rooms emails notice boards groups]','{as=1, boards=1, cause=1, chate=1, commission=1, commit=1, computer=3, crime=4, crimes=1, criminal=1, cyber=2, defined=1, directly=1, emails=1, groups=2, harm=2, includ=1, indirectly=1, individuals=2, intentionally=1, internet=1, involves=1, limited=1, loss=1, mental=1, modern=1, motive=1, networks=2, networkthe=1, notice=1, offences=1, oriented=1, physic=1, reputate=1, rooms=1, target=1, telecommunicate=1, use=1, used=1, victim=2}','Crime'),(6,'child gromming.txt','73717878','Child grooming is befriending and establishing an emotional connection with a child, and sometimes the family, to lower the childs inhibitions with the objective of sexual abuse. Child grooming is also regularly used to lure minors into various illicit businesses such as child trafficking, child prostitution, or the production of child pornography.','child grooming befriending establishing emotional connection child sometimes family lower childs inhibitions objective sexual abuse child grooming regularly used lure minors various illicit businesses child trafficking child prostitution production child pornography','[child groome befriend establishing emotion connection child sometimes family lower childs inhibitions objective sexual abuse child groome regularly used lure minors various illicit businesses child trafficking child prostitution production child pornography]','{abuse=1, befriend=1, businesses=1, child=6, childs=1, connection=1, emotion=1, establishing=1, family=1, groome=2, illicit=1, inhibitions=1, lower=1, lure=1, minors=1, objective=1, pornography=1, production=1, prostitution=1, regularly=1, sexual=1, sometimes=1, trafficking=1, used=1, various=1}','Crime');

/*Table structure for table `filereq` */

DROP TABLE IF EXISTS `filereq`;

CREATE TABLE `filereq` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `fkey` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT 'De-Active',
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `filereq` */

/*Table structure for table `highlevel` */

DROP TABLE IF EXISTS `highlevel`;

CREATE TABLE `highlevel` (
  `filename` varchar(100) DEFAULT NULL,
  `rank` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `highlevel` */

insert  into `highlevel`(`filename`,`rank`) values ('Os.txt','2'),('single and multiuser.txt','3'),('single tasking.txt','2'),('system software.txt','1');

/*Table structure for table `his` */

DROP TABLE IF EXISTS `his`;

CREATE TABLE `his` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `uname` varchar(200) DEFAULT NULL,
  `keyw` varchar(200) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `typ` varchar(200) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `his` */

insert  into `his`(`id`,`uname`,`keyw`,`fname`,`typ`) values (1,'bhuvi','hack ','No Result','RSTP'),(2,'bhuvi','operating ','[single and multiuser.txt, Os.txt, single tasking.txt, system software.txt]','STP');

/*Table structure for table `idf` */

DROP TABLE IF EXISTS `idf`;

CREATE TABLE `idf` (
  `terms` varchar(100) DEFAULT NULL,
  `d1` varchar(100) DEFAULT NULL,
  `d2` varchar(100) DEFAULT NULL,
  `d3` varchar(100) DEFAULT NULL,
  `dd1` varchar(100) DEFAULT NULL,
  `dd2` varchar(100) DEFAULT NULL,
  `dd3` varchar(100) DEFAULT NULL,
  `tf1` varchar(100) DEFAULT NULL,
  `tf2` varchar(100) DEFAULT NULL,
  `tf3` varchar(100) DEFAULT NULL,
  `q` varchar(100) DEFAULT NULL,
  `idfdash` varchar(100) DEFAULT NULL,
  `idf` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `idf` */

/*Table structure for table `rare` */

DROP TABLE IF EXISTS `rare`;

CREATE TABLE `rare` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `word` varchar(200) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `rare` */

insert  into `rare`(`id`,`word`) values (1,'attack'),(2,'kill'),(3,'spoof'),(4,'fraud'),(5,'hack');

/*Table structure for table `reg` */

DROP TABLE IF EXISTS `reg`;

CREATE TABLE `reg` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `pass` varchar(200) DEFAULT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `mobile` varchar(200) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `reg` */

insert  into `reg`(`id`,`name`,`pass`,`fname`,`lname`,`email`,`mobile`) values (3,'bhuvi','bhuvi','bhuvi','bhuvi','bhuvana.vlsa@gmail.com','7708150152'),(4,'priya','priya','priya','priya','priya@gmail.com','7708150152');

/*Table structure for table `result` */

DROP TABLE IF EXISTS `result`;

CREATE TABLE `result` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `fname` varchar(200) DEFAULT NULL,
  `count` int(200) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `result` */

insert  into `result`(`id`,`fname`,`count`) values (1,'Os.txt',2),(2,'single and multiuser.txt',3),(3,'single tasking.txt',2),(4,'system software.txt',1);

/*Table structure for table `tdash` */

DROP TABLE IF EXISTS `tdash`;

CREATE TABLE `tdash` (
  `terms` varchar(100) DEFAULT NULL,
  `d1` varchar(100) DEFAULT NULL,
  `d2` varchar(100) DEFAULT NULL,
  `d3` varchar(100) DEFAULT NULL,
  `dd1` varchar(100) DEFAULT NULL,
  `dd2` varchar(100) DEFAULT NULL,
  `dd3` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tdash` */

/*Table structure for table `tf` */

DROP TABLE IF EXISTS `tf`;

CREATE TABLE `tf` (
  `terms` varchar(100) DEFAULT NULL,
  `d1` varchar(100) DEFAULT NULL,
  `d2` varchar(100) DEFAULT NULL,
  `d3` varchar(100) DEFAULT NULL,
  `dd1` varchar(100) DEFAULT NULL,
  `dd2` varchar(100) DEFAULT NULL,
  `dd3` varchar(100) DEFAULT NULL,
  `tf1` varchar(100) DEFAULT NULL,
  `tf2` varchar(100) DEFAULT NULL,
  `tf3` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tf` */

/*Table structure for table `val` */

DROP TABLE IF EXISTS `val`;

CREATE TABLE `val` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `fname` varchar(200) DEFAULT NULL,
  `ke` varchar(200) DEFAULT NULL,
  `count` int(200) DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `val` */

insert  into `val`(`id`,`fname`,`ke`,`count`) values (1,'Os.txt','operating',2),(2,'single and multiuser.txt','operating',3),(3,'single tasking.txt','operating',2),(4,'system software.txt','operating',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
