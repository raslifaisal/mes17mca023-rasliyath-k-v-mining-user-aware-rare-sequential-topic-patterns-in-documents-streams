<%-- 
    Document   : ownerhome
    Created on : Mar 9, 2017, 1:40:53 PM
    
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">		
		<title>Sequential Topic Patterns</title>
		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Arvo:400,700|" rel="stylesheet" type="text/css">
		<link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Loading main css file -->
		<link rel="stylesheet" href="style.css">
	</head>
	<body>		
		<div id="site-content">
			<header class="site-header">
				<div class="primary-header">
					<div class="container">						
						<div class="main-navigation">
							<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
							<ul class="menu">								
                                                                <li class="menu-item"><a href="ownerhome.jsp">Admin Home</a></li>
                                                                <li class="menu-item"><a href="new.jsp">Users</a></li>
                                                                <li class="menu-item"><a href="upload.jsp">Upload</a></li>  
                                                                <li class="menu-item"><a href="stp.jsp">STP</a></li>
                                                                <li class="menu-item"><a href="rstp.jsp">RSTP</a></li>
                                                               <li class="menu-item"><a href="logout.jsp">Logout</a></li>
							</ul> <!-- .menu -->
						</div> <!-- .main-navigation -->

						<div class="mobile-navigation"></div>
					</div> <!-- .container -->
				</div> <!-- .primary-header -->
				<div class="home-slider">
					<div class="container">
						<div class="slider">
							<ul class="slides">
								<li>
									<div class="slide-caption">
										<h2 class="slide-title" style="font-size: 40px;">Mining User-Aware Rare Sequential <br> Topic Patterns in Document Streams</h2>
									</div>									
								</li>								
							</ul><br><br>
						</div>
					</div>
				</div>
			</header>
		</div>

		<main class="main-content">
			<div class="fullwidth-block">
				<div class="container">
					<div class="row">
                                            <center>
                                            <h1>Admin Home</h1>	
                                            </center>
					</div>
				</div>
			</div>
		</main>
		<footer class="site-footer">
			<div class="container">
				<div class="row">
					<h1>Abstract</h1>
                                        <p style="text-align: justify;color: white;">People use Internet for different purposes e.g. socialnetworking, blogging etc. with respect to their context. Thisleads to dynamic change in creation and distribution of document streams over the Internet. This would challenge the topicmodelling and evolution of individual topics. In this paper, wehave proposed Sequential Topic Patterns (STPs) mining overthe published user-aware document streams and formulate theproblem of mining UserAware Rare Sequential Topic Patterns(URSTPs) in document streams on the Internet in order to findrare users. They are generally rare and infrequent over theInternet. For URSTPs mining we need to perform three phases:pre-processing to extract topics, generating STPs, determiningURSTPs by rarity analysis of STPs. The experiment can be performed on both real times (Twitter) and synthetic data-sets.Inthe proposed work, we have focused on synthetic datasets.</p>
				</div>
			</div>

		</footer>
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>		
	</body>

</html>